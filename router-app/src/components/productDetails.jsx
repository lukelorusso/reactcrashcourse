import React, { Component } from "react";

class ProductDetails extends Component {
  handleSave = () => {
    //this.props.history.push("/products"); // keeps the history
    this.props.history.replace("/products"); // LOGIN CASE: won't keep the history
  };

  render() {
    return (
      <div>
        <h1>Product Details - {this.props.match.params.id}</h1>
        <button onClick={this.handleSave}>Save</button>
      </div>
    );
  }
}

export default ProductDetails;
