import React, { Component } from "react";
import NavBar from "./components/navbar";
import Counters from "./components/counters";
import "./App.css";

class App extends Component {
  state = {
    counters: [
      { id: 0, value: 0 },
      { id: 1, value: 0 },
      { id: 2, value: 0 },
      { id: 3, value: 4 }
    ]
  };

  constructor() {
    super();
    console.log("App constructor");
  }

  componentDidMount() {
    console.log("App mounted");
  }

  handleReset = () => {
    this.setState({
      counters: this.state.counters.map(c => {
        c.value = 0;
        return c;
      })
    });
  };

  handleDecrement = counter => {
    const newCounters = [...this.state.counters];
    const id = newCounters.indexOf(counter);
    if (newCounters[id].value === 0) return;
    newCounters[id] = { ...counter };
    newCounters[id].value--;
    this.setState({
      counters: newCounters
    });
    console.log("Decremented #" + id);
  };

  handleIncrement = counter => {
    const newCounters = [...this.state.counters];
    const id = newCounters.indexOf(counter);
    newCounters[id] = { ...counter };
    newCounters[id].value++;
    this.setState({
      counters: newCounters
    });
    console.log("Incremented #" + id);
  };

  handleDelete = id => {
    //console.log("Deleted #" + id);
    this.setState({
      counters: this.state.counters.filter(c => c.id !== id)
    });
  };

  render() {
    console.log("App rendered");
    return (
      <React.Fragment>
        <NavBar
          totalCounters={this.state.counters.filter(c => c.value > 0).length}
        />
        <main className="container">
          <Counters
            counters={this.state.counters}
            onReset={this.handleReset}
            onDecrement={this.handleDecrement}
            onIncrement={this.handleIncrement}
            onDelete={this.handleDelete}
          />
        </main>
      </React.Fragment>
    );
  }
}

export default App;
