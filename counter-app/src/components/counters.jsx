import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    // this will avoid "this.props" repetitions
    const {
      onReset,
      counters,
      onDelete,
      onDecrement,
      onIncrement
    } = this.props;

    console.log("Counters rendered");

    return (
      <div>
        <button onClick={onReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onDecrement={onDecrement}
            onIncrement={onIncrement}
            counter={counter}
          />
        ))}
      </div>
    );
  }
}

export default Counters;
