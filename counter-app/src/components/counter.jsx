import React, { Component } from "react";

class Counter extends Component {
  componentDidUpdate(prevProps, prevState) {
    console.log("Counter updated", prevProps, prevState);
  }

  componentWillUnmount() {
    console.log("Counter unmounted");
  }

  render() {
    console.log("Counter rendered");

    const tdStyle = {
      width: "60px",
      textAlign: "center"
    };

    return (
      <div>
        <table>
          <tr>
            <td>
              <button
                onClick={() => this.props.onDecrement(this.props.counter)}
                className="btn btn-secondary btn-sm"
                disabled={this.props.counter.value === 0}
              >
                -
              </button>
            </td>
            <td style={tdStyle}>
              <span style={{ fontSize: 12 }} className={this.getBadgeClasses()}>
                {this.formatCount()}
              </span>
            </td>
            <td>
              <button
                onClick={() => this.props.onIncrement(this.props.counter)}
                className="btn btn-secondary btn-sm"
              >
                +
              </button>
            </td>
            <td>
              <button
                onClick={() => this.props.onDelete(this.props.counter.id)}
                className="btn btn-danger btn-sm m-2"
              >
                Delete
              </button>
            </td>
          </tr>
        </table>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.isCountZero() ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    return this.isCountZero() ? "Zero" : this.props.counter.value;
  }

  isCountZero() {
    return this.props.counter.value === 0;
  }
}

export default Counter;
