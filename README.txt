The first part of this code is composed by the following folders:
./react-app/
./counter-app/

those has been inspired by:

https://www.youtube.com/watch?v=Ke90Tje7VS0

The second part is done by following this online course:

https://codewithmosh.com/p/mastering-react

---
First run:

before doing anything:
npm install -g create-react-app

then "CD" to a project and:
npm install

to start that project:
npm start

to generate a production build:
npm run build

once the WebApp is built, you may serve it with a static server:
npm install -g serve
serve -s build
