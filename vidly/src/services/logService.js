function init() {
  // here you might config your logging service
}

function log(error) {
  // to better logging and tracking errors use Raven.js for Sentry
  console.log(error);
}

export default { init, log };
