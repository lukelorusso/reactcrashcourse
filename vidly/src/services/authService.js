import http from "./httpService";
import jwtDecode from "jwt-decode";

const apiEndpoint = "/auth";
const tokenKey = "jwtoken";

http.setJwt(getJwToken());

export async function login(email, password) {
  const { data: jwtoken } = await http.post(apiEndpoint, {
    email,
    password
  });
  this.loginWithJwt(jwtoken);
}

export function loginWithJwt(jwtoken) {
  localStorage.setItem(tokenKey, jwtoken);
}

export function logout() {
  localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
  try {
    return jwtDecode(localStorage.getItem(tokenKey));
  } catch (ex) {
    return null;
  }
}

export function getJwToken() {
  return localStorage.getItem(tokenKey);
}

export default { login, loginWithJwt, logout, getCurrentUser, getJwToken };
