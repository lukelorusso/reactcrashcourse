import React from "react";
import TableHeader from "./tableHeader";
import TableBody from "./tableBody";

const Table = ({ columns, sortColumn, onSorted, data }) => {
  return (
    <table className="table">
      <TableHeader
        columns={columns}
        sortColumn={sortColumn}
        onSorted={onSorted}
      />
      <TableBody data={data} columns={columns} />
    </table>
  );
};

export default Table;
