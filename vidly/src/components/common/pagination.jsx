import React from "react";
import _ from "lodash";
import PropTypes from "prop-types";

const Pagination = ({
  itemsCount,
  pageSize,
  currentPageNumber,
  onPageChanged
}) => {
  const pagesCount = Math.ceil(itemsCount / pageSize);

  if (pagesCount <= 1) return null;

  // let's see lodash in action:
  const pages = _.range(1, pagesCount + 1);

  return (
    <nav>
      <ul className="pagination">
        {pages.map(page => (
          <li
            key={page}
            className={
              page === currentPageNumber ? "page-item active" : "page-item"
            }
          >
            <a className="page-link" onClick={() => onPageChanged(page)}>
              {page}
            </a>
          </li>
        ))}
      </ul>
    </nav>
  );
};

Pagination.propTypes = {
  itemsCount: PropTypes.number.isRequired,
  pageSize: PropTypes.number.isRequired,
  currentPageNumber: PropTypes.number.isRequired,
  onPageChanged: PropTypes.func.isRequired
};

export default Pagination;
