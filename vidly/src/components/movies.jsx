import React, { Component } from "react";
import { toast } from "react-toastify";
import { Link } from "react-router-dom";
import movieModel from "../models/movieModel";
import { getMovies, deleteMovie, saveMovie } from "../services/movieService";
import { getGenres } from "../services/genreService";
import SearchBox from "./common/searchBox";
import Pagination from "./common/pagination";
import MoviesTable from "./moviesTable";
import { paginate } from "../utils/paginate";
import { ListGroup } from "./common/listGroup";
import _ from "lodash";

class Movies extends Component {
  state = {
    movies: [],
    genres: [],
    pageSize: 4,
    currentPageNumber: 1,
    sortColumn: { path: "title", order: "asc" },
    searchQuery: ""
  };

  async componentDidMount() {
    const { data: g } = await getGenres();
    const genres = [{ _id: "", name: "All Genres" }, ...g];

    const { data: m } = await getMovies();
    this.setState({
      movies: m,
      genres,
      selectedGenre: genres[0]
    });
  }

  handleLike = async movie => {
    // also call the DB in the future!
    const movies = [...this.state.movies];
    const id = movies.indexOf(movie);
    movies[id].liked = !movies[id].liked;
    try {
      let data = movieModel.mapToViewModel(movie);
      await saveMovie(data);
      this.setState({ movies });
    } catch (ignored) {}
  };

  handleDelete = async movie => {
    const originalData = this.state.movies;
    const movies = originalData.filter(m => m._id !== movie._id);
    this.setState({ movies });

    try {
      await deleteMovie(movie._id);
    } catch (ex) {
      if (ex.response && ex.response.status === 404)
        toast.error("This movie has already been deleted");
      this.setState({ movies: originalData });
    }
  };

  handlePageChanged = page => {
    this.setState({ currentPageNumber: page });
  };

  handleGenreSelected = genre => {
    this.setState({
      currentPageNumber: 1,
      selectedGenre: genre,
      searchQuery: ""
    });
  };

  handleSearch = query => {
    this.setState({
      currentPageNumber: 1,
      selectedGenre: this.state.genres[0],
      searchQuery: query
    });
  };

  handleSort = sortColumn => {
    this.setState({ sortColumn });
  };

  pageData = (
    originalData,
    selectedGenre,
    sortColumn,
    currentPageNumber,
    pageSize,
    searchQuery
  ) => {
    let data;
    if (searchQuery) {
      data = originalData.filter(m =>
        m.title.toLowerCase().includes(searchQuery.toLowerCase())
      );
    } else {
      data = originalData;
    }
    const filteredData = this.filterData(data, selectedGenre);
    const sortedData = this.sortData(filteredData, sortColumn);
    data = paginate(sortedData, currentPageNumber, pageSize);
    return {
      count: sortedData.length,
      data
    };
  };

  filterData = (data, selectedGenre) => {
    return selectedGenre && selectedGenre._id
      ? data.filter(m => m.genre._id === selectedGenre._id)
      : data;
  };

  sortData = (data, sortColumn) => {
    return _.orderBy(data, [sortColumn.path], [sortColumn.order]);
  };

  render() {
    const {
      pageSize,
      currentPageNumber,
      movies,
      genres,
      selectedGenre,
      sortColumn,
      searchQuery
    } = this.state;

    const { user } = this.props;

    const { count, data } = this.pageData(
      movies,
      selectedGenre,
      sortColumn,
      currentPageNumber,
      pageSize,
      searchQuery
    );

    return (
      <div className="row">
        <div className="col-3">
          <ListGroup
            items={genres}
            selectedItem={selectedGenre}
            onItemSelected={this.handleGenreSelected}
          />
        </div>
        <div className="col">
          {user && (
            <Link
              to="/movies/new"
              className="btn btn-primary"
              style={{ marginBottom: 20 }}
            >
              New Movie
            </Link>
          )}
          <p>Showing {count < 1 ? 0 : count} movies</p>
          <SearchBox value={searchQuery} onChange={this.handleSearch} />
          <MoviesTable
            movies={data}
            sortColumn={sortColumn}
            onLikeClicked={this.handleLike}
            onDeleteClicked={this.handleDelete}
            onSorted={this.handleSort}
          />
          <Pagination
            itemsCount={count}
            onPageChanged={this.handlePageChanged}
            currentPageNumber={currentPageNumber}
            pageSize={pageSize}
          />
        </div>
      </div>
    );
  }
}

export default Movies;
