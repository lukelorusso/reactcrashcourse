import axios from "axios";
import { toast } from "react-toastify";

axios.interceptors.response.use(null, error => {
  // null is the eventual response interceptor, now we don't need it
  const expectedError =
    error.response &&
    error.response.status >= 400 &&
    error.response.status < 500;

  if (!expectedError) {
    // Unexpected (network down, server down, DB down, server bug)
    // - Log them
    // - Display a generic and user friendly error message
    console.log("Error", error); // to better logging and tracking errors use Raven.js for Sentry
    toast.error("Unexpected error occurred");
  }

  return Promise.reject(error);
});

export default {
  get: axios.get,
  post: axios.post,
  put: axios.put,
  patch: axios.patch,
  delete: axios.delete
};
